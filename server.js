var express 		= require('express'),
	app 			= express(),
	mongoose 		= require('mongoose'),
	morgan			= require('morgan'),
	bodyParser		= require('body-parser'),
	methodOverride	= require('method-override'),
	multer 			= require('multer'),
	port			= process.env.PORT || 3000;
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
var mongoose   = require('mongoose');
app.use(multer({ dest: './uploads/'}));
mongoose.connect('mongodb://localhost:27017/Iganiq8o'); // connect to our database


// Setup route
var router = express.Router();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

router.use(function(req,res,next){
	//do logging

	console.log(Date());
	next();
});
// Upload block
// we need the fs module for moving the uploaded files
var fs = require('fs');
app.post('/file-upload', function(req, res) {
    // get the temporary location of the file
    var tmp_path = req.files.thumbnail.path;
    // set where the file should actually exists - in this case it is in the "images" directory
    // var target_path = './public/images/' + req.files.thumbnail.name;
    var target_path = './public/images/' + req.files.thumbnail.originalname;
 	var public_path = '/images/'+ req.files.thumbnail.originalname;
 
    console.log('file being uploaded ' + target_path + ' uploaded name : ' + req.files.thumbnail.originalname);
    // rename from tmp name to uploaded name
    // move the file from the temporary location to the intended location
    fs.rename(tmp_path, target_path, function(err) {
        if (err) throw err;
        // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
        fs.unlink(tmp_path, function() {
            if (err) throw err;
            res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes' + '<br /><img src="' + public_path + '" />');
        });
    });
});
// End of upload block
// test upload jig
app.route('/upload').post(function(req,res,next){
	console.log(req.body);
	console.log(req.files);
});
// end of upload jig
app.use('/api', router);
// defatult router
app.use('*',function(req,res){
	res.sendfile('./public/index.html');
});

app.listen(port);
console.log("App listening on port " + port);