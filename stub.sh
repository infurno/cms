#!/bin/bash
# This shell script will buid out stubs for each topic that we want to build for a cms. 
# hal@sfpixels.co
# 12/29/2014   #GOHOGS #HornsDown
STUBABUB=$1
WORKDIR=public/$STUBABUB
JSWORK=public/js/
echo "#### Createing new scafffolding  ####"
mkdir $WORKDIR
touch $WORKDIR/{edit,delete,main}.html
#### delete template
cat << EOF > $WORKDIR/delete.html
This is the delete template
EOF
#### edit template
cat << EOF > $WORKDIR/edit.html
This is the edit template
EOF
#### main template
cat << EOF > $WORKDIR/main.html
<div ng-controller="${STUBABUB}Controller">
	<h4>{{ section }}</h4>
		<div>{{ name }}<br />

		<button class="radius" ng-click="addRow()">add new</button>
	</div>
	<div class="row">
	<table width="100%">
	  <thead>
	    <tr>
	      <th >Some Header</th>
	      <th >Action</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr ng-repeat="rowContent in rows">
	      <td>{{ rowContent || 'empty '}}</td><td><a href="#/edit/{{counter}}" class="button radius">Edit</a></td>
	    </tr>
	  </tbody>
	</table>    
	</div>
</div>
EOF
#### Setup new controller.js for stub
touch $JSWORK/${STUBABUB}Controller.js
cat << EOF > $JSWORK/${STUBABUB}Controller.js
app.controller('${STUBABUB}Controller', function(\$scope){
	\$scope.section = "${STUBABUB} management";
		\$scope.name = "${STUBABUB}";
		\$scope.rows = ['Edit me'];
		\$scope.counter = 2 ;
		\$scope.addRow = function(){
			\$scope.rows.push('New Item' + \$scope.counter);
			\$scope.counter++;
		};
});
app.controller('${STUBABUB}EditController', function(\$scope){
	var id = \$scope.id;
		\$scope.project = \$scope.projects[id];

		console.log(\$scope.project);
		\$scope.save = function(){

			//save to db
			console.log('saving to db with rest request');
			console.log('\$scope.project to database');

			\$location.path('/');

		};

});
EOF
### Setup New router.js for stub
cat << EOF > $JSWORK/${STUBABUB}Router.js
//start of ${STUBABUB}Router.js
app.config(function(\$routeProvider){
	\$routeProvider.when('/${STUBABUB}',{
		templateUrl: '${STUBABUB}/main.html',
		controller: '${STUBABUB}Controller'
	}).when('/${STUBABUB}/edit/:id',{
		templateUrl: '${STUBABUB}/edit.html',
		controller: '${STUBABUB}EditController'
	});
});

//end of ${STUBABUB}Router.js
EOF
#### Setup new edit form for stub
cat << EOF > $WORKDIR/edit.html
<div ng-controller="${STUBABUB}EditController">
<p>Edit Project</p>
<form action="form">
	<label for="name" >Name</label>
	<input type="text" ng-model="${STUBABUB}.model">
	{{project.names}}

	<label for="desc" >Description</label>
	<textarea name="" id="" cols="30" rows="10" ng-model="project.desc"></textarea>
	<button class="button">Save</button>
	<button class="button alert">Cancel</button>
</form>
</div>

EOF