//start of eventsRouter.js
app.config(function($routeProvider){
	$routeProvider.when('/events',{
		templateUrl: 'events/main.html',
		controller: 'eventsController'
	}).when('/events/edit/:id',{
		templateUrl: 'events/edit.html',
		controller: 'eventsEditController'
	});
});

//end of eventsRouter.js
