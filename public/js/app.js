// Bootstrap APP
var app = angular.module('App',['ngRoute','mm.foundation']);
	//////////////////////  routing //////////////////////
	app.config(function($routeProvider){
		$routeProvider.when('/',{
			templateUrl: 'main.html'
		}).when('/edit/:id',{
			templateUrl: 'edit.html',
			controller: 'EditController'
		}).when('/productions/',{
			templateUrl: 'productions/main.html',
			controller: 'ProductionController'
		}).when('/productions/edit/:id',{
			templateUrl: 'productions/edit.html',
			controller: 'ProductionsEditController'
		});

	});
	//////////////////////  routing \\\\\\\\\\\\\\\\\\\\\\
	////////////////////// Main Controller \\\\\\\\\\\\\\\\\\\\\\
	app.controller('Ctrl',function($scope){
				$scope.section = "Main Dashboard";
		$scope.name = "Home Page";
		$scope.projects ={
			1:{
				id:1,
				name: 'Les Misérables',
				desc: 'The Lyceum premiere of the epic and uplifting story about the survival of the human spirit seen by 65 million people worldwide. Set against the backdrop of 19th-century France, LES MISÉRABLES is an unforgettable story of heartbreak, passion, and the resilience of the human spirit, that has become one of the most celebrated musicals in theatrical history. Featuring the timeless score and beloved songs “I Dreamed A Dream,” “Bring Him Home,” “One Day More,” and “On My Own,” Epic, grand and uplifting, LES MISÉRABLES packs an emotional wallop that has thrilled audiences all over the world. MUSICAL PG-13'
			},
			2:{
				id:2,
				name: 'South Pacific',
				desc: 'Social tensions and exotic romance collide in Rodgers and Hammerstein’s groundbreaking musical tale set in a Polynesian paradise threatened by the dangers of prejudice and war. Based on a book by James Michener, South Pacific tells the sweeping romantic story of two couples as they search for happiness through trying times and differing backgrounds. With a majestic score including "Bali Ha’i," "I’m Gonna Wash That Man Right Outta My Hair," "Some Enchanted Evening," "There is Nothing Like a Dame,"  and "Younger Than Springtime," South Pacific has earned its place as one of the greatest musicals of all time. MUSICAL PG' 
			}
		}
	});
	/////////  Edit controller get parms from url  \\\\\\\\\	
	app.controller('EditController', function($scope, $routeParams, $location){
		var id = $routeParams.id;
		$scope.project = $scope.projects[id];

		console.log($scope.project);
		$scope.save = function(){

			//save to db
			console.log('saving to db with rest request');
			console.log('$scope.project to database');

			$location.path('/');

		};

	});
	//////////////////////  Production Controller \\\\\\\\\\\\\\\\\\\\\\
	app.controller('ProductionController',function($scope){
		$scope.section = "Productions";
		$scope.name = "This is the name";
		$scope.rows = ['FOO', 'BOB'];

		$scope.counter= 3;
		$scope.addRow = function(){
			$scope.rows.push('New Item' + $scope.counter);
			$scope.counter++;
		};
	});
	/////////  Production Edit controller get parms from url  \\\\\\\\\	

	app.controller('ProductionEditController', function($scope, $routeParams, $location){
		var id = $routeParams.id;
		$scope.section = "Productions";
		$scope.name = "This is the Edit by id " + id;
		console.log("foooooooooo");

	});

