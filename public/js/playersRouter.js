//start of playersRouter.js
app.config(function($routeProvider){
	$routeProvider.when('/players',{
		templateUrl: 'players/main.html',
		controller: 'playersController'
	}).when('/players/edit/:id',{
		templateUrl: 'players/edit.html',
		controller: 'playersEditController'
	});
});

//end of playersRouter.js
