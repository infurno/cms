//start of pagesRouter.js
app.config(function($routeProvider){
	$routeProvider.when('/pages',{
		templateUrl: 'pages/main.html',
		controller: 'pagesController'
	}).when('/pages/edit/:id',{
		templateUrl: 'pages/edit.html',
		controller: 'pagesEditController'
	});
});

//end of pagesRouter.js
