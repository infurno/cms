var app = angular.module('App',['ngRoute']);
//////////////////////  routing //////////////////////
app.config(function($routeProvider){
	$routeProvider.when('/',{
		templateUrl: 'main.html'
	}).when('/edit/:id',{
		templateUrl: 'edit.html',
		controller: 'EditController'
	}).when('/productions/',{
		templateUrl: 'productions/main.html',
		controller: 'ProductionController'
	}).when('/productions/edit/:id',{
		templateUrl: 'productions/edit.html',
		controller: 'ProductionsEditController'
	});
});
	