//start of sponsorsRouter.js
app.config(function($routeProvider){
	$routeProvider.when('/sponsors',{
		templateUrl: 'sponsors/main.html',
		controller: 'sponsorsController'
	}).when('/sponsors/edit/:id',{
		templateUrl: 'sponsors/edit.html',
		controller: 'sponsorsEditController'
	});
});

//end of sponsorsRouter.js
