//start of subpagesRouter.js
app.config(function($routeProvider){
	$routeProvider.when('/subpages',{
		templateUrl: 'subpages/main.html',
		controller: 'subpagesController'
	}).when('/subpages/edit/:id',{
		templateUrl: 'subpages/edit.html',
		controller: 'subpagesEditController'
	});
});

//end of subpagesRouter.js
